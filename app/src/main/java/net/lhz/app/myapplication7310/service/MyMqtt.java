package net.lhz.app.myapplication7310.service;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import net.lhz.app.myapplication7310.entity.MyMqttMessage;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MyMqtt {

    public final String TAG = "MQTT";
    private MqttAndroidClient mqttAndroidClient;
    private MqttConnectOptions mMqttConnectOptions;
    public String MQTT_HOST = "tcp://82.157.131.46:1883";//服务器地址（协议+地址+端口号）
    public String MQTT_UserName = "xingmeng";              // 用户名
    public String MQTT_PassWord = "xingmeng";             // 密码
    public int MQTT_ConnectionTimeout = 10;             // 设置超时时间，单位：秒
    public int KeepAliveIntervalTime = 20;              // 设置心跳包发送间隔，单位：秒
    public boolean CleanSession = true;                 // 设置是否清除缓存
    public String Subscribe_Topic = "Music";      // 客户端订阅主题
    public String EndWillMsgPublish_Topic = "EndWillMsg";// 遗嘱发布主题
    public static String RESPONSE_TOPIC = "message_arrived";//响应主题

    public String CLIENTID = "testiiiiii";
    private Handler MQTThandler;

    // 初始化
    public void init(Context context, Handler MQTThandlerS) {
        MQTThandler = MQTThandlerS;
        String serverURI = MQTT_HOST; //服务器地址（协议+地址+端口号）
        mqttAndroidClient = new MqttAndroidClient(context, serverURI, CLIENTID);
        mqttAndroidClient.setCallback(mqttCallback); //设置监听订阅消息的回调
        mMqttConnectOptions = new MqttConnectOptions();
        mMqttConnectOptions.setCleanSession(CleanSession); //设置是否清除缓存
        mMqttConnectOptions.setConnectionTimeout(MQTT_ConnectionTimeout); //设置超时时间，单位：秒
        mMqttConnectOptions.setKeepAliveInterval(KeepAliveIntervalTime); //设置心跳包发送间隔，单位：秒
        mMqttConnectOptions.setUserName(MQTT_UserName); //设置用户名
        mMqttConnectOptions.setPassword(MQTT_PassWord.toCharArray()); //设置密码

        // last will message
        boolean doConnect = true;
        String message = "{\"terminal_uid\":\"" + CLIENTID + "\",\"msg\":\"Client offline\"}";
        // 最后的遗嘱
        try {
            mMqttConnectOptions.setWill(EndWillMsgPublish_Topic, message.getBytes(), 2, false);
        } catch (Exception e) {
            Log.e(TAG, "Exception Occured", e);
            doConnect = false;
        }
        if (doConnect) {
            doClientConnection();
        }
    }


    // 连接MQTT服务器
    private void doClientConnection() {
        if (!mqttAndroidClient.isConnected()) {
            try {
                mqttAndroidClient.connect(mMqttConnectOptions, null, new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        MQTT_Subscribe(Subscribe_Topic);
                        MQTT_Subscribe("androidTopic2");
                        System.out.println("连接mqtt服务器成功");

                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        System.out.println("连接mqtt服务器失败");
                    }
                });
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    // 订阅主题的回调
    private MqttCallback mqttCallback = new MqttCallback() {
        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            Log.e(TAG, "收到消息MQTT： " + topic + " 发来的消息 ：" + new String(message.getPayload()));
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken arg0) {

        }

        @Override
        public void connectionLost(Throwable arg0) {
            Log.e(TAG, "MQTT 服务器连接断开 ");
            doClientConnection();//连接断开，重连
        }
    };

    // 订阅该主题
    private void MQTT_Subscribe(String Publish_Topic) {
        Boolean retained = false;// 是否在服务器保留断开连接后的最后一条消息
        try {
            mqttAndroidClient.subscribe(Publish_Topic, 0, null, new IMqttActionListener() {
                //订阅成功
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    System.out.println("订阅成功");
                }

                //订阅失败
                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    System.out.println("订阅失败" + exception);
                }
            });

            //接收返回的信息（对返回的内容进行相关的操作）
            mqttAndroidClient.subscribe(Publish_Topic, 0, new IMqttMessageListener() {
                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    Message msg = new Message();
                    MyMqttMessage myMqttMessage = new MyMqttMessage(topic, message.toString());
                    msg.obj = myMqttMessage;
                    MQTThandler.sendMessage(msg);

                    System.out.println("主题：" + topic);
                    System.out.println("消息：" + message);
                }
            });

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    // 取消订阅
    public void MQTT_UnSubscribe(String Publish_Topic) {
        Boolean retained = false;// 是否在服务器保留断开连接后的最后一条消息
        try {
            mqttAndroidClient.unsubscribe(Publish_Topic);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    // 向相关主题发布消息
    public void MQTT_Publish(String Publish_Topic, Integer qos, String message) {
        Boolean retained = false;// 是否在服务器保留断开连接后的最后一条消息
        if(mqttAndroidClient==null){
            Log.e(TAG, "MQTT_Publish: client=null");
            return;
        }
        try {
            //参数分别为：主题、消息的字节数组、服务质量、是否在服务器保留断开连接后的最后一条消息
            mqttAndroidClient.publish(Publish_Topic, message.getBytes(), qos, retained.booleanValue());
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


}
