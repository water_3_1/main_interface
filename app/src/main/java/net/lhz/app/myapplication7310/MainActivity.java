package net.lhz.app.myapplication7310;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import net.lhz.app.myapplication7310.entity.MyMqttMessage;
import net.lhz.app.myapplication7310.service.MyMqtt;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import android.os.Handler;

public class MainActivity extends AppCompatActivity{
    ImageView imageView2;
    ImageButton mTextView;
    TextView DAte;
    RequestQueue requestQueue;
    private static final String TAG = "MainActivity";


  private TextView yy;


    private ImageButton sys,zt,xys;
    MyMqtt myMqtt;
    private Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        yy=(TextView) findViewById(R.id.YY);
        sys=(ImageButton) findViewById(R.id.SYS);
        zt=(ImageButton) findViewById(R.id.ZT);
        xys=(ImageButton) findViewById(R.id.XYS);


        myMqtt = new MyMqtt();
        context = MainActivity.this;
        myMqtt.init(context, MQTTHandler);



        //上一首
        sys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myMqtt.MQTT_Publish("MusicCMD", 0, "{CMD:1}");
            }
        });
        //下一首
        xys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myMqtt.MQTT_Publish("MusicCMD", 0, "{CMD:2}");
            }
        });
        //暂停/播放
        zt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //调用音乐模块接口进行控制

                //同步到手机端
                myMqtt.MQTT_Publish("MusicCMD", 0, "{CMD:3}");

            }
        });



        DAte = findViewById(R.id.tv_time);
        //导航按钮调用
        findViewById(R.id.button_navi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setComponent(new ComponentName("com.amap.navi.demo", "com.amap.navi.demo.activity.IndexActivity"));
                startActivity(i);
            }
        });

        //音乐按钮调用

        findViewById(R.id.button_mu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setComponent(new ComponentName("com.example.simplemusic", "com.example.simplemusic.activity.StartActivity"));
                startActivity(i);
            }
        });


        //状态按钮调用
        findViewById(R.id.button_st).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setComponent(new ComponentName("com.neusoft.qiangzi.carstatues", "com.neusoft.qiangzi.carstatues.MainActivity"));
                startActivity(i);
            }
        });


        SimpleDateFormat sdf = new SimpleDateFormat(" HH:mm");//设置日期格式
        Date now = new Date();
        String time = sdf.format(now);
        System.out.println(time);
        DAte.setText(time);

        imageView2 = findViewById(R.id.imageView2);
        requestQueue = Volley.newRequestQueue(this);

        //创建JS请求
        requestHttp();
    }


    public Handler MQTTHandler = new Handler() {
        public void handleMessage(Message msg) {
            //取出消息中的数据
            MyMqttMessage myMqttMessage = (MyMqttMessage) msg.obj;
            String topic=myMqttMessage.getTopic().trim();
            String message=myMqttMessage.getMessage().trim();

            if("Music".equals(topic)){


//JSON字符串转换成JSON对象
                //JSONObject jsonObject1 = JSONObject.parseObject(stuString);
                try {
                    JSONObject MyJSON = new JSONObject( message.toString());
                    String zzbf = MyJSON.getString("title");
                    if(zzbf != null){
                        yy.setText(zzbf);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    private void requestHttp() {
        JsonObjectRequest request = new JsonObjectRequest(
                "http://wthrcdn.etouch.cn/weather_mini?city="+"大连",
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        //接收到应答数据
                        Log.d(TAG,"onResponse: "+response.toString());

                        try {
                            JSONObject o = response.getJSONObject("data");
                            String city = o.getString("city");
                            String wendu = o.getString("wendu");
                            JSONArray arr = o.getJSONArray("forecast");
                            o = arr.getJSONObject(0);
                            String date = o.getString("date");
                            String high = o.getString("high");
                            String low = o.getString("low");
                            String fengxiang = o.getString("fengxiang");
                            String type = o.getString("type");

                            Log.d(TAG,"onResponse: city"+city);
                            Log.d(TAG,"onResponse: wendu"+wendu);
                            Log.d(TAG, "onResponse：date "+date);
                            Log.d(TAG, "onResponse：high "+high);
                            Log.d(TAG, "onResponse：low "+low);
                            Log.d(TAG, "onResponse：fengxiang "+fengxiang);
                            Log.d(TAG, "onResponse：type "+type);
//                    Looper.prepare();
                            //更新UI控件
                            TextView tvTianqi = findViewById(R.id.weather);
                            TextView tvWendu = findViewById(R.id.temperature);
                            TextView tvCity = findViewById(R.id.city);

                            tvTianqi.setText(type);
                            tvWendu.setText(wendu);
                            tvCity.setText(city);

                            if(type.equals("晴")){
                                imageView2.setImageResource(R.drawable.sunny);
                            }
                            if(type.equals("多云")){
                                imageView2.setImageResource(R.drawable.sunnycloudy);
                            }
                            if(type.equals("阴")){
                                imageView2.setImageResource(R.drawable.cloudy);
                            }
                            if(type.equals("中雨")){
                                imageView2.setImageResource(R.drawable.rainy);
                            }
                            if(type.equals("小雨")){
                                imageView2.setImageResource(R.drawable.rainy);
                            }
                            if(type.equals("大雨")){
                                imageView2.setImageResource(R.drawable.rainy);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG,"onErrorResponse: "+ error.toString());
                    }
                }

        );
        //请求添加到队列
        requestQueue.add(request);
    }

    private void show(){
        this.startActivity(new Intent(this,MainActivity2.class));
    }

}


